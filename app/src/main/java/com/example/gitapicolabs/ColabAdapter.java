package com.example.gitapicolabs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class ColabAdapter extends ArrayAdapter {

       private Context context;
       private ArrayList<ColabData> data;

       public ColabAdapter(Context context, int resource, ArrayList<ColabData> data) {
           super(context, resource, data);

           this.context=context;
           this.data=data;

       }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null){
            viewHolder = new ViewHolder();
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.custom_layout, parent, false);
            viewHolder.colabName = convertView.findViewById(R.id.login);
            viewHolder.colabID = convertView.findViewById(R.id.id);
            viewHolder.colabImg = convertView.findViewById(R.id.avatar_url);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder=(ViewHolder)convertView.getTag();
        }

        viewHolder.colabName.setText(data.get(position).getLogin());
        viewHolder.colabID.setText("ID: "+data.get(position).getId());

        Glide.with(context)
                .load(data.get(position).getAvatar_url())
                .override(50, 50)
                .into(viewHolder.colabImg);

        return convertView;

        }

    static class ViewHolder{
           TextView colabName;
           TextView colabID;
           CircleImageView colabImg;
    }
}

