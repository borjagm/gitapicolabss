package com.example.gitapicolabs;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
 //COMENTARIO
public class MainActivity extends AppCompatActivity {
    private ListView colabListView;
    private static final String JSON="https://api.github.com/repos/android10/Android-CleanArchitecture/contributors";
    private ArrayList<ColabData> colabDataArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        colabListView = findViewById(R.id.list);

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(JSON, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                parseJSON(response);
                manejarJSON();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Error");
            }
        });

        Volley.newRequestQueue(MainActivity.this).add(jsonArrayRequest);
    }


    private void parseJSON(JSONArray response){
        colabDataArrayList= new ArrayList<>();
        for (int count = 0; count<response.length(); count++){
            try{
                JSONObject jsonObject = response.getJSONObject(count);
                String colabName = jsonObject.getString("login");
                String colabID = jsonObject.getString("id");
                String urlImg = jsonObject.getString("avatar_url");
                System.out.println(colabName);
                System.out.println(colabID);
                System.out.println(urlImg);

                colabDataArrayList.add(new ColabData(colabName, colabID, urlImg));
            }catch (JSONException ex){
                System.out.println ("Error en JSON");
            }
        }

    }

    private void manejarJSON() {

        ColabAdapter colabAdapter = new ColabAdapter(MainActivity.this, R.layout.custom_layout, colabDataArrayList);
        colabListView.setAdapter(colabAdapter);

    }


}
